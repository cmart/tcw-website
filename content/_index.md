---
---

![a Javelina made of CSS](img/javelina-cropped.jpg)

Tucson Code Works is a tiny (180 square foot) semi-private tech-focused coworking space in downtown Tucson, Arizona. The main goal of TCW is to provide _a nice, friendly workplace_ for technologists who are otherwise isolated from colleagues.

Amenities include monitors, office plants, nerdy coffee/tea gear, and a small book collection.

## Can I join?

It's subject to availability of space and how compatible your habits are with a small shared office. The ideal members...

- are friendly and personable, but respectful of others' need to focus.
  - Think 10% banter, 90% getting stuff done.
- don't need to spend hours talking on work calls.
  - It's okay to take occasional calls in the space, and there is also a conference room you can use if unoccupied.
- are respectful to others in the space regardless of their personal characteristics or identity.
  - (Basically, any harrassment or similar behavior that would get you in trouble with a competent HR department is also not welcome here.)
- are conscientious about their impact in a shared space, clean up their food / crumbs / trash, etc.

If you're visiting Tucson and want to join as a short-term guest, hit us up, we might be able to accommodate.

If you're local and want to join as an occasional or full-time member, we may invite you to join as a guest for a few days. If this trial goes well, we may offer you an ongoing membership.

In either case, send a message to info at-sign tucsoncode dot works . Give us a few sentences about yourself, your work, and what you want from a shared workspace.

If you just want to stop by and meet us, that's cool too.

## What does it cost?

- $250/month membership includes a desk dedicated to you (IKEA IDÅSEN frame with solid bamboo top).
- $100/month membership to use any unoccupied desk, plus your own fob for building access.
- Free to join as a guest, but you need to arrange it in advance.

## What are the other local coworking options?

[La Suprema](https://liquidspace.com/us/az/tucson/la-suprema-works-events) near the convention center. [The L Offices](https://www.theloffices.com/) has two locations (though none near downtown). IWG has several locations under various names. A few other places on the edges of the city that I know very little about. :)

Common, Connect, Brings, and CoLab are all defunct.

For a friendly but still working-oriented weekly gathering, visit [Code Commons](https://codecommons.net) on Wednesday afternoons at UA Main Library. (May not meet during the summer.)

For a more sociable but still tech-focused gathering, join [ResBaz Arizona](https://researchbazaar.arizona.edu/#events)'s excellent weekly events: Coffee and Code, and Hacky Hour.

## Where is it?

64 E. Broadway Blvd, on the second floor.

## How do I visit?

The exterior door is normally locked, so one of us needs to come downstairs and let you in.

The most reliable way is to arrange it in advance via a message to info at-sign tucsoncode dot works .

To visit on short notice, try calling the following phone number: siete uno seis - six zero three - V VIII IX 0. If you don't get an answer, call twice in a row to puncture do-not-disturb.

## Who is in charge?

TCW is organized by me, Chris Martin. Among other things, I also organize [Code Commons](https://codecommons.net).